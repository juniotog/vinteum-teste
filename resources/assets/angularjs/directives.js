angular.module('directives', [])
.directive("ngFileSelect",function(){    
    return {
      link: function($scope,el){          
        el.bind("change", function(e){          
          $scope.getFile((e.srcElement || e.target).files[0]);
        });          
      }        
    }
})
.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9-]/g, '');
                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
})
.directive('dropDown', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attr, ngModel) {
      $(element).on("click","li a", function(){
        var _this = $(this);
        var _parent = _this.parent();
        var _submenu = _parent.find('ul.treeview-menu');

        if (_submenu.css('display') == 'none') {
          _parent.addClass('menu-open');
        } else {
          _parent.removeClass('menu-open');
        }

        _submenu.slideToggle();
      });
    }
  };
})
.directive('cep', ['$parse', '$http', '$timeout', function($parse, $http, $timeout) {
  'use strict';
  return {
    require: 'ngModel',
    link: function(scope, element, attrs) {

      var searchCep = false;
      var model = attrs.ngModel.replace('.cep', '');
      var cepid = attrs.id.replace('_cep', '');


      scope.$watch(attrs.ngModel, function(newValue, oldValue) {
        
        if(newValue != oldValue) {
          searchCep = false;
        }
        if (angular.isDefined(newValue) && newValue.length == 8) {
            pesquisarCep();
        }
      });
      element.keypress(function(e){
          if(e.keyCode == 13)
          {
              e.preventDefault();
              pesquisarCep();
              return false;
          }
      }).blur(function() {
          pesquisarCep();
      });
      function pesquisarCep() {
        
        $http.get('http://api.postmon.com.br/v1/cep/' + scope.$eval(model).cep, {
            headers: {
                'Content-Type': undefined,
                'Authorization': undefined
            }
        })
        .then(
          function(response) {
            scope.$eval(model).logradouro = response.data.logradouro;
            scope.$eval(model).bairro = response.data.bairro;
            scope.$eval(model).logradouro = response.data.logradouro;
            scope.estados = [{
                descricao: response.data.estado_info.nome,
                codigo_ibge: response.data.estado_info.codigo_ibge
            }];
            scope.cidades = [{
                descricao: response.data.cidade,
                codigo_ibge: response.data.cidade_info.codigo_ibge
            }];
            scope.$eval(model).estado_id = response.data.estado_info.codigo_ibge;
            scope.$eval(model).cidade_id = response.data.cidade_info.codigo_ibge;
            
            $timeout(function() {
                $("#numero").focus();
            });
          },
          function() {
            PNotify.error({
                text: "CEP inválido ou não existe!"
              })
          });
      }
    }
  };
}]);
  