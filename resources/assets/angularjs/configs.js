angular.module('configs', [])
.config(function($routeProvider, $interpolateProvider, cfpLoadingBarProvider){
  $interpolateProvider.startSymbol('<%').endSymbol('%>');
  cfpLoadingBarProvider.spinnerTemplate = '<div id="overlay"><img src="/images/ajax-loading-mini.gif"></div>';
  $routeProvider
  .when('/imoveis', {
    templateUrl: '/views/imoveis/index.html',
    controller: 'ImovelListagemController',
    controllerAs: 'vm',
  })
  .when('/imoveis/create', {
    templateUrl: '/views/imoveis/create.html',
    controller: 'ImovelCadastroController',
    controllerAs: 'vm',
  })
  .when('/imoveis/import-xml', {
    templateUrl: '/views/imoveis/import.html',
    controller: 'ImovelImportarController',
    controllerAs: 'vm',
  })
  .when('/imoveis/:id/edit', {
    templateUrl: '/views/imoveis/edit.html',
    controller: 'ImovelEditarController',
    controllerAs: 'vm',
  })
  .when('/login', {
    templateUrl: '/views/login/login.html',
    controllerAs: 'vm',
    controller: 'LoginController'
  })
})
.run(['$rootScope', '$location', '$http',
  function ($rootScope, $location, $http) {
      var credentials = sessionStorage.getItem("credentials");

      if(credentials !== null && credentials.length > 3) {
          var credential = JSON.parse(credentials);

          $rootScope.credentials = credential || {};
          if ($rootScope.credentials.access_token) {
            
              $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.credentials.access_token;
          }
      }

  $rootScope.$on('$locationChangeStart', function (event, next, current) {
      // redirect to login page if not logged in
      if ($location.path() !== '/login' && !$rootScope.credentials) {
          $location.path('/login');
      }
  });
}]);
  