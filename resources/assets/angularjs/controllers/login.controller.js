'use strict';

/* Controllers */
angular.module('controllers.login', [])
.controller('LoginController', ['$scope', '$http', 'AutenticarService', '$location', 'UsuarioService',
function ($scope, $http, AutenticarService, $location, UsuarioService) {
    var vm = this;
    
    vm.submit = submit;

    vm.auth = {
        error: {
            message: null
        },
        email: null,
        password: null,
    }
    

    function submit() {
        var email = vm.auth.email;
        var password = vm.auth.password;

        AutenticarService.Login(email, password, function (response) {
            console.log(response);
            if (response.success) {
                AutenticarService.SetCredentials(response.response.data);
                UsuarioService.Usuario(function(usuario) {
                    UsuarioService.SetUsuario(usuario);
                });
                $location.path('/');
            } else {
                vm.auth.error.message = 'As credenciais do usuário estavam incorretas.';
            }
        });
    }
}]);