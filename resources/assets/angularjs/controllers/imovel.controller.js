'use strict';

/* Controllers */
angular.module('controllers.imovel', [])
.controller('ImovelImportarController', ['$scope', '$http', 'ImovelService',
function ($scope, $http, ImovelService) {
    var vm = this;

    vm.importar = function () {
        vm.data = {
            arquivo: angular.element(document.getElementById("arquivo"))
        };
        ImovelService.import(vm.data).then(function (response) {
            PNotify.success({
                text: response.data.mensagem
            });
        });
    }
    
}])
.controller('ImovelEditarController', ['$scope', '$http', 'ImovelService', '$routeParams', '$timeout', 'ImagemService',
function ($scope, $http, ImovelService, $routeParams, $timeout, ImagemService) {
    var vm = this;
    vm.imovel = {
        imovel_tipo_id: 1,
        imovel_status_id: 1,
        images: [],
    };
    vm.data = [];

    vm.editar = function () {
        ImovelService.update($routeParams.id, vm.imovel).then(function(response) {
            PNotify.success({
                text: "Imóvel editado com sucesso!"
            });
        });
    }

    ImovelService.getData().then(function(response) {
        vm.data = response.data;
    });

    vm.removeImg = function (index) {
        vm.imovel.images.splice(index, 1);
    }

    $scope.getFile = function (file) {
        $scope.progress = 0;
        ImagemService.readAsDataUrl(file, $scope).then(function(result) {
            vm.imovel.images.push({descricao: result}); 
            $("input[type=file]").val("");
        });
    };

    ImovelService.getById($routeParams.id).then(function(response) {
        vm.imovel = response.data.dados;
    });
}])
.controller('ImovelCadastroController', ['$scope', '$http', 'ImovelService', 'ImagemService',
function ($scope, $http, ImovelService, ImagemService) {
    var vm = this;
    vm.imovel = {
        imovel_tipo_id: 1,
        imovel_status_id: 1,
        images: [],
    };
    vm.data = [];

    $scope.getFile = function (file) {
        $scope.progress = 0;
        ImagemService.readAsDataUrl(file, $scope).then(function(result) {
            vm.imovel.images.push({descricao: result}); 
            $("input[type=file]").val("");
        });
    };

    ImovelService.getData().then(function(response) {
        vm.data = response.data;
    });

    vm.removeImg = function (index) {
        vm.imovel.images.splice(index, 1);
    }
    vm.cadastrar = function () {

        ImovelService.store(vm.imovel).then(function() {
            PNotify.success({
                text: "Imóvel cadastrado com sucesso!"
              });
        });
    }
}])
.controller('ImovelListagemController', ['$scope', '$http', 'ImovelService',
function ($scope, $http, ImovelService) {
    var vm = this;
    

    vm.imoveis = [];
    vm.paginate = {};
    vm.idbuscar = null;

    vm.deletarImovel = function (imovel) {
        ImovelService.delete(imovel.id).then (function (response) {
            var index = vm.imoveis.indexOf(imovel);
            vm.imoveis.splice(index, 1);
            PNotify.success({
                text: response.data.mensagem
            });
        });
    };

    vm.getImoveis = function (page, id) {
        ImovelService.getAll(page, id).then (function (response) {
            vm.imoveis = response.data.dados.data;
            delete response.data.dados.data;
            vm.paginate = response.data.dados;
        });
    };

    vm.buscarPorId = function(current_page) {
        if (angular.isDefined(vm.idbuscar)) {
            vm.getImoveis(current_page, vm.idbuscar);
        }
    };
}]);