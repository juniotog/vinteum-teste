'use strict';

/* Controllers */
angular.module('controllers.main', [])
.controller('MainController', ['$scope', '$http', 'AutenticarService', '$location',
function ($scope, $http, AutenticarService, $location) {

    $scope.usuario = JSON.parse(sessionStorage.getItem("usuario"));

    $scope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
    }
    $scope.getNumber = function(num) {
        return new Array(num);   
    }

    $scope.Sair = function() {
        AutenticarService.ClearCredentials();
        $location.path('/login');
    };
}]);