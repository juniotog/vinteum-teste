angular.module('VinteumApp.controllers', [])
.config(function($mdIconProvider) {
  $mdIconProvider
    .icon('share-arrow', 'img/icons/share-arrow.svg', 24)
    .icon('upload', 'img/icons/upload.svg', 24)
    .icon('copy', 'img/icons/copy.svg', 24)
    .icon('print', 'img/icons/print.svg', 24)
    .icon('hangout', 'img/icons/hangout.svg', 24)
    .icon('mail', 'img/icons/mail.svg', 24)
    .icon('message', 'img/icons/message.svg', 24)
    .icon('copy2', 'img/icons/copy2.svg', 24)
    .icon('facebook', 'img/icons/facebook.svg', 24)
    .icon('twitter', 'img/icons/twitter.svg', 24);
})
.controller('AppCtrl', ['$scope', '$mdBottomSheet','$mdSidenav', '$mdDialog', function($scope, $mdBottomSheet, $mdSidenav, $mdDialog){
  $scope.toggleSidenav = function(menuId) {
    $mdSidenav(menuId).toggle();
  };
 	$scope.menu = [
    {
      link : '/dashboard',
      title: 'Dashboard',
      icon: 'dashboard'
    },
    {
      link : '/imoveis',
      title: 'Imoveis',
      icon: 'home'
    },
  ];
 
  $scope.activity = [
      {
        what: 'Brunch this weekend?',
        who: 'Ali Conners',
        when: '3:08PM',
        notes: " I'll be in your neighborhood doing errands"
      },
      {
        what: 'Summer BBQ',
        who: 'to Alex, Scott, Jennifer',
        when: '3:08PM',
        notes: "Wish I could come out but I'm out of town this weekend"
      },
      {
        what: 'Oui Oui',
        who: 'Sandra Adams',
        when: '3:08PM',
        notes: "Do you have Paris recommendations? Have you ever been?"
      },
      {
        what: 'Birthday Gift',
        who: 'Trevor Hansen',
        when: '3:08PM',
        notes: "Have any ideas of what we should get Heidi for her birthday?"
      },
      {
        what: 'Recipe to try',
        who: 'Brian Holt',
        when: '3:08PM',
        notes: "We should eat this: Grapefruit, Squash, Corn, and Tomatillo tacos"
      },
    ];
  $scope.alert = '';
  
  $scope.showGridBottomSheet = function() {
    $scope.alert = '';
    $mdBottomSheet.show({
      templateUrl: '/js/templates/sheetgrid.html',
      controller: 'GridBottomSheetCtrl',
      clickOutsideToClose: false
    }).then(function(clickedItem) {
      $mdToast.show(
            $mdToast.simple()
              .textContent(clickedItem['name'] + ' clicked!')
              .position('top right')
              .hideDelay(1500)
          );
    }).catch(function(error) {
      // User clicked outside or hit escape
    });
  };

}])
.controller('GridBottomSheetCtrl', function($scope, $mdBottomSheet) {
  $scope.items = [
    { name: 'Hangout', icon: 'hangout' },
    { name: 'Mail', icon: 'mail' },
    { name: 'Message', icon: 'message' },
    { name: 'Copy', icon: 'copy2' },
    { name: 'Facebook', icon: 'facebook' },
    { name: 'Twitter', icon: 'twitter' },
  ];

  $scope.listItemClick = function($index) {
    var clickedItem = $scope.items[$index];
    $mdBottomSheet.hide(clickedItem);
  };
}).run(function($templateRequest) {

  var urls = [
    'img/icons/share-arrow.svg',
    'img/icons/upload.svg',
    'img/icons/copy.svg',
    'img/icons/print.svg',
    'img/icons/hangout.svg',
    'img/icons/mail.svg',
    'img/icons/message.svg',
    'img/icons/copy2.svg',
    'img/icons/facebook.svg',
    'img/icons/twitter.svg'
  ];

  angular.forEach(urls, function(url) {
    $templateRequest(url);
  });

});