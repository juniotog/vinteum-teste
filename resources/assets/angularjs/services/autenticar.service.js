'use strict';

/* Services */
angular.module('services.autenticar', [])
.factory('AutenticarService', ['$rootScope', '$http', function ($rootScope, $http) {
    var service = {};

    service.Login = Login;
    service.SetCredentials = SetCredentials;
    service.ClearCredentials = ClearCredentials;

    return service;

    function Login(email, password, callback) {

        $http.post('/oauth/token', {
            grant_type: "password",
            client_id: "2",
            client_secret: "V20PgsE31F5P4trHl3LwPAZ3oHZX8zztdTLr73WC",
            username: email,
            password: password,

        }).then(function (response) {
            callback({
                response: response,
                success: true
            });
        }, function(response) {
            callback({
                response: response,
                success: false
            });
        });
    }

    function SetCredentials(token) {
        $rootScope.credentials = token;
        sessionStorage.setItem("credentials", JSON.stringify($rootScope.credentials));
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + token.access_token;    
    }

    function ClearCredentials() {
        delete $rootScope.credentials;
        sessionStorage.removeItem("credentials");
        $http.defaults.headers.common.Authorization = 'Bearer';
    }
}]);