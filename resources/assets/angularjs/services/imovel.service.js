'use strict';

/* Controllers */
angular.module('services.imovel', [])
.service('ImovelService', ['$rootScope', '$http', 'upload', function ($rootScope, $http, upload) {
   
    this.getData = function () {
        return $http.get("/api/imoveis/create");
    };

    this.getAll = function (page, id) {
        return $http.get("/api/imoveis", {
            params: { page: page, id: id }
        });
    };

    this.store = function (data) {
        return $http.post("/api/imoveis", data);
    };

    this.import = function (data) {
        return upload({url: '/api/imoveis/import', method: 'POST', data});
    };

    this.delete = function (id) {
        return $http.delete("/api/imoveis/"+id);
    };
    
    this.getById = function (id) {
        return $http.get("/api/imoveis/"+id);
    };

    this.update = function (id, data) {
        return $http.put("/api/imoveis/"+id, data);
    };
}]);

