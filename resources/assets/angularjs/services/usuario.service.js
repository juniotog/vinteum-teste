'use strict';

/* Controllers */
angular.module('services.usuario', [])
.factory('UsuarioService', ['$rootScope', '$http', function ($rootScope, $http) {
    var service = {};

    service.Usuario = Usuario;
    service.SetUsuario = SetUsuario;
    

    return service;

    function Usuario(callback) {
        $http.get('/api/usuario').then(function(response) {
            callback(response.data);
        });
    }

    function SetUsuario(usuario) {
        sessionStorage.setItem("usuario", JSON.stringify(usuario));
    }
}]);