angular.module('VinteumApp', [
   'ngRoute',
   'angular-loading-bar',
   'configs',
   'controllers.login',
   'controllers.main',
   'controllers.imovel',
   'services.autenticar',
   'services.usuario',
   'services.imovel',
   'admin-lte-angular',
   'ui.utils.masks',
   'directives',
   'services.imagem',
   'lr.upload'
]);