<?php

namespace VinteUm\Repositories;

abstract class BaseRepository
{
    /**
     * The Model instance.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Destroy a model.
     *
     * @param  int $id
     * @return void
     */


    public function destroy($id)
    {
        $this->getById($id)->delete();
    }
    /**
     * Get Model by id.
     *
     * @param  int  $id
     * @return \VinteUm\Models\Model
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function find($id) {
        return $this->model->find($id);
    }

    /**
     * Get all data model
     * @return Array Model
     */
    public function getAll() {
        return $this->model->all();
    }

    public function update($data, $id) {
        $model = $this->model->find($id)->fill($data);

        return ( $model->update() ) ? $model : false;
    }

    public function store($data) {
        return $this->model->create($data);
    }
}