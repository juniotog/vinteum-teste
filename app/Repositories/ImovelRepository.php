<?php

namespace VinteUm\Repositories;

use VinteUm\Entities\Imovel;

class ImovelRepository extends BaseRepository
{
    public function __construct(Imovel $model) {
        $this->model = $model;
    }

    public function getById($id) {
        return $this->model->with(['imovel_status', 'imovel_tipo', 'cidade.estado', 'images'])->findOrFail($id);
    }

    public function getAllImoveis($id) {
        $imoveis = $this->model->with(['imovel_status', 'imovel_tipo', 'cidade.estado']);
        if (!is_null($id)) {
            $imoveis->where('id', $id);
        }
        return $imoveis->paginate(15);
    }

    public function update($data, $id) {
        $imovel = $this->model->find($id)->fill($data);
        $imovel->update();
        if (count($data['images']) > 0) {
            $imovel->images()->delete();
            $imovel->images()->createMany($data['images']);
        }
        return ( $imovel ) ? $imovel : false;
    }

    public function store($data) {
        $imovel = $this->model->create($data);
        if (count($data['images']) > 0) {
            $imovel->images()->createMany($data['images']);
        }
        return $imovel;
    }
}