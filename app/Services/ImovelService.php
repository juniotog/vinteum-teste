<?php

namespace VinteUm\Services;

use VinteUm\Repositories\ImovelRepository;

class ImovelService
{
    private $repository;

    public function __construct(ImovelRepository $repository) {
        $this->repository = $repository;
    }

    public function getAllImoveis($id) {
        return $this->repository->getAllImoveis($id);
    }

    public function destroy($id) {
        return $this->repository->destroy($id);
    }

    public function getById($id) {
        return $this->repository->getById($id);
    }

    public function dataForm() {
        return [
            'imovel_status' => \VinteUm\Entities\ImovelStatus::all(),
            'imovel_tipo' => \VinteUm\Entities\ImovelTipo::all(),
        ];
    }

    public function importXml($data) {
        $xml = \XmlParser::load($data['arquivo']->getRealPath());
        $imoveis = $xml->parse([
            'Imovel' => ['uses' => 'Imoveis.Imovel[TipoImovel>tipo_imovel,Observacao>descricao,Cidade>cidade_id,Bairro>bairro,Numero>numero,Complemento>complemento,CEP>cep,PrecoVenda>preco_venda,PrecoLocacao>preco_locacao,PrecoLocacaoTemporada>preco_temporada,AreaTotal>area,QtdDormitorios>dormitorios,QtdSuites>suites,QtdBanheiros>banheiros,QtdVagas>garagem,QtdSalas>salas,Fotos.Foto.URLArquivo]', 'default' => null],
            
        ]);

        foreach($imoveis['Imovel'] as $key => $imovel) {
            if ($imovel['tipo_imovel']) {
                switch(strtolower($imovel['tipo_imovel'])) {
                    case 'casa':
                        $imoveis['Imovel'][$key]['imovel_tipo_id'] = \VinteUm\Entities\ImovelTipo::CASA;
                    break;
                    case 'apartamento':
                        $imoveis['Imovel'][$key]['imovel_tipo_id'] = \VinteUm\Entities\ImovelTipo::AP;
                    break;
                    default:
                        $imoveis['Imovel'][$key]['imovel_tipo_id'] = \VinteUm\Entities\ImovelTipo::OUTROS;
                    break;
                 }
            }
            if ($imovel['cidade_id']) {
                $cidade = \VinteUm\Entities\Cidade::where('descricao', 'like', '%'.$imovel['cidade_id'].'%')->first();
                if(is_null($cidade)) {
                    $imoveis['Imovel'][$key]['cidade_id'] = 592;
                } else {
                    $imoveis['Imovel'][$key]['cidade_id'] = $cidade->id;
                }
            }
            if (!is_numeric($imovel['numero'])) {
                $imoveis['Imovel'][$key]['numero'] = 0;
            }

            if ($imovel['cep']) {
                $imoveis['Imovel'][$key]['cep'] = str_replace("-", null, $imoveis['Imovel'][$key]['cep']);
            }

            if ($imovel['preco_venda'] !== "0") {
                $imoveis['Imovel'][$key]['preco'] = str_replace(".", null, $imoveis['Imovel'][$key]['preco_venda']);
                $imoveis['Imovel'][$key]['imovel_status_id'] = \VinteUm\Entities\ImovelStatus::VENDA;
            }

            if ($imovel['preco_locacao'] !== "0") {
                $imoveis['Imovel'][$key]['preco'] = str_replace(".", null, $imoveis['Imovel'][$key]['preco_locacao']);
                $imoveis['Imovel'][$key]['imovel_status_id'] = \VinteUm\Entities\ImovelStatus::LOCACAO;
            }

            if ($imovel['preco_temporada'] !== "0") {
                $imoveis['Imovel'][$key]['preco'] = str_replace(".", null, $imoveis['Imovel'][$key]['preco_temporada']);
                $imoveis['Imovel'][$key]['imovel_status_id'] = \VinteUm\Entities\ImovelStatus::TEMPORADA;
            }
            if (strlen($imovel['Fotos']['Foto']['URLArquivo']) > 0) {
                /*
                $data = file_get_contents($imovel['Fotos']['Foto']['URLArquivo']);
                $base64 = 'data:image/jpg;base64,' . base64_encode($data);
                $imoveis['Imovel'][$key]['images'] = [$base64];
                */
            }
            unset($imoveis['Imovel'][$key]['Fotos']);
            unset($imoveis['Imovel'][$key]['preco_venda']);
            unset($imoveis['Imovel'][$key]['tipo_imovel']);
            unset($imoveis['Imovel'][$key]['preco_locacao']);
            unset($imoveis['Imovel'][$key]['preco_temporada']);
        }
        
        \DB::table('imovel')->insert($imoveis['Imovel']);
        return true;
    }

    public function update($data, $id) {

        $cidade = \VinteUm\Entities\Cidade::where(
            'codigo_ibge', $data['cidade_id']
        )->first();

        $data['cidade_id'] =  $cidade->id;

        return  $this->repository->update($data, $id);
    }

    public function store($data) {
        $cidade = \VinteUm\Entities\Cidade::where(
            'codigo_ibge', $data['cidade_id']
        )->first();
        $data['cidade_id'] =  $cidade->id;

       return $this->repository->store(
            $data
        );
    }
}