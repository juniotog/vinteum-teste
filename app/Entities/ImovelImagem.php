<?php

namespace VinteUm\Entities;

use Illuminate\Database\Eloquent\Model;

class ImovelImagem extends Model
{
    protected $table = 'imovel_imagem';
    protected $fillable = ['id', 'descricao', 'imovel_id'];
    public $timestamps = false;
}
