<?php

namespace VinteUm\Entities;

use Illuminate\Database\Eloquent\Model;

class ImovelStatus extends Model
{
    protected $table = 'imovel_status';
    const VENDA = 1;
    const LOCACAO = 2;
    const TEMPORADA = 3;
}
