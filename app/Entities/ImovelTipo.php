<?php

namespace VinteUm\Entities;

use Illuminate\Database\Eloquent\Model;

class ImovelTipo extends Model
{
    protected $table = 'imovel_tipo';
    const CASA = 1;
    const AP = 2;
    const OUTROS = 3;
}
