<?php

namespace VinteUm\Entities;

use Illuminate\Database\Eloquent\Model;

class Imovel extends Model
{
    protected $table = 'imovel';

    protected $fillable = ['id','titulo' ,'imovel_tipo_id' ,'cidade_id' ,'cep' ,'preco','area' ,'dormitorios','suites' ,'banheiros' ,'salas' ,'garagem' ,'descricao','bairro' ,'numero' ,'complemento','logradouro','imovel_status_id'];

    public function imovel_status()
    {
        return $this->belongsTo(ImovelStatus::class);
    }

    public function imovel_tipo()
    {
        return $this->belongsTo(ImovelTipo::class);
    }

    public function cidade()
    {
        return $this->belongsTo(Cidade::class);
    }

    public function images()
    {
        return $this->hasMany(ImovelImagem::class);
    }
}
