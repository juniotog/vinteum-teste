<?php

namespace VinteUm\Http\Controllers;

use Illuminate\Http\Request;

use VinteUm\Services\ImovelService;

use VinteUm\Http\Requests\Imovel\StoreImovel;
use VinteUm\Http\Requests\Imovel\UpdateImovel;



class ImovelController extends Controller
{
    private $service;

    public function __construct(ImovelService $service) {
        $this->service = $service;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return [
            'dados' => $this->service->getAllImoveis($request->id)
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->service->dataForm();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreImovel $request)
    {

        $dados = $this->service->store($request->all());

        return [
            'mensagem' => 'Imóvel criado com sucesso',
            'dados' => $dados
        ];
    }

    
    /**
     * Import a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {

        $dados = $this->service->importXml($request->all());

        return [
            'mensagem' => 'Imóvel criado com sucesso',
            'dados' => null
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return [
            'dados' => $this->service->getById($id)
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateImovel $request, $id)
    {
        $dados = $this->service->update($request->all(), $id);

        return [
            'mensagem' => 'Imóvel editado com sucesso',
            'dados' => $dados
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->service->destroy($id);

        return [
            'mensagem' => 'Imóvel deletado com sucesso'
        ];
    }
}
