<?php

namespace VinteUm\Http\Requests\Imovel;

use VinteUm\Http\Requests\FormRequest;

class StoreImovel extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|max:45',
            'imovel_tipo_id' => 'required|exists:imovel_tipo,id',
            'cidade_id' => 'required|exists:cidade,codigo_ibge',
            'cep' => 'required|size:8',
            'area' => 'required|numeric',
            'dormitorios' => 'required|numeric',
            'suites' => 'required|numeric',
            'banheiros' => 'required|numeric',
            'salas' => 'required|numeric',
            'garagem' => 'required|numeric',
            'descricao' => 'nullable',
            'bairro' => 'required|max:150',
            'numero' => 'required|numeric',
            'complemento' => 'nullable|max:45',
            'logradouro' => 'required|max:255',
            'imovel_status_id' => 'required|exists:imovel_status,id',
        ];
    }
}
