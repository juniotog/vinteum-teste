<?php

use Illuminate\Database\Seeder;

class ImovelStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imovel_status')->truncate();
        DB::table('imovel_status')->insert([
            [
                'id' => 1,
                'descricao' => 'Venda'
            ],
            [
                'id' => 2,
                'descricao' => 'Locação'
            ],
            [
                'id' => 3,
                'descricao' => 'Temporada'
            ]
        ]);
    }
}