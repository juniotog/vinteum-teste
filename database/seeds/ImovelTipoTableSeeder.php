<?php

use Illuminate\Database\Seeder;

class ImovelTipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imovel_tipo')->truncate();
        DB::table('imovel_tipo')->insert([
            [
                'id' => 1,
                'descricao' => 'Casa'
            ],
            [
                'id' => 2,
                'descricao' => 'Apartamento'
            ],
            [
                'id' => 3,
                'descricao' => 'Outros'
            ]
        ]);
    }
}
