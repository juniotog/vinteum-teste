<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado')->truncate();
        DB::table('estado')->insert([
            [
                'id' => '1',
                
                'descricao' => 'Acre',
                'sigla' => 'AC',
                'codigo_ibge' => '12'
            ],
            [
                'id' => '2',
                
                'descricao' => 'Alagoas',
                'sigla' => 'AL',
                'codigo_ibge' => '27'
            ],
            [
                'id' => '3',
                
                'descricao' => 'AmapÃ¡',
                'sigla' => 'AP',
                'codigo_ibge' => '16'
            ],
            [
                'id' => '4',
                
                'descricao' => 'Amazonas',
                'sigla' => 'AM',
                'codigo_ibge' => '13'
            ],
            [
                'id' => '5',
                
                'descricao' => 'Bahia',
                'sigla' => 'BA',
                'codigo_ibge' => '29'
            ],
            [
                'id' => '6',
                
                'descricao' => 'CearÃ¡',
                'sigla' => 'CE',
                'codigo_ibge' => '23'
            ],
            [
                'id' => '7',
                
                'descricao' => 'Distrito Federal',
                'sigla' => 'DF',
                'codigo_ibge' => '53'
            ],
            [
                'id' => '8',
                
                'descricao' => 'EspÃ­rito Santo',
                'sigla' => 'ES',
                'codigo_ibge' => '32'
            ],
            [
                'id' => '9',
                
                'descricao' => 'GoiÃ¡s',
                'sigla' => 'GO',
                'codigo_ibge' => '52'
            ],
            [
                'id' => '10',
                
                'descricao' => 'MaranhÃ£o',
                'sigla' => 'MA',
                'codigo_ibge' => '21'
            ],
            [
                'id' => '11',
                
                'descricao' => 'Mato Grosso',
                'sigla' => 'MT',
                'codigo_ibge' => '51'
            ],
            [
                'id' => '12',
                
                'descricao' => 'Mato Grosso do Sul',
                'sigla' => 'MS',
                'codigo_ibge' => '50'
            ],
            [
                'id' => '13',
                
                'descricao' => 'Minas Gerais',
                'sigla' => 'MG',
                'codigo_ibge' => '31'
            ],
            [
                'id' => '14',
                
                'descricao' => 'ParÃ¡',
                'sigla' => 'PA',
                'codigo_ibge' => '15'
            ],
            [
                'id' => '15',
                
                'descricao' => 'ParaÃ­ba',
                'sigla' => 'PB',
                'codigo_ibge' => '25'
            ],
            [
                'id' => '16',
                
                'descricao' => 'ParanÃ¡',
                'sigla' => 'PR',
                'codigo_ibge' => '41'
            ],
            [
                'id' => '17',
                
                'descricao' => 'Pernambuco',
                'sigla' => 'PE',
                'codigo_ibge' => '26'
            ],
            [
                'id' => '18',
                
                'descricao' => 'PiauÃ­',
                'sigla' => 'PI',
                'codigo_ibge' => '22'
            ],
            [
                'id' => '19',
                
                'descricao' => 'Rio de Janeiro',
                'sigla' => 'RJ',
                'codigo_ibge' => '33'
            ],
            [
                'id' => '20',
                
                'descricao' => 'Rio Grande do Norte',
                'sigla' => 'RN',
                'codigo_ibge' => '24'
            ],
            [
                'id' => '21',
                
                'descricao' => 'Rio Grande do Sul',
                'sigla' => 'RS',
                'codigo_ibge' => '43'
            ],
            [
                'id' => '22',
                
                'descricao' => 'RondÃ´nia',
                'sigla' => 'RO',
                'codigo_ibge' => '11'
            ],
            [
                'id' => '23',
                
                'descricao' => 'Roraima',
                'sigla' => 'RR',
                'codigo_ibge' => '14'
            ],
            [
                'id' => '24',
                
                'descricao' => 'Santa Catarina',
                'sigla' => 'SC',
                'codigo_ibge' => '42'
            ],
            [
                'id' => '25',
                
                'descricao' => 'SÃ£o Paulo',
                'sigla' => 'SP',
                'codigo_ibge' => '35'
            ],
            [
                'id' => '26',
                
                'descricao' => 'Sergipe',
                'sigla' => 'SE',
                'codigo_ibge' => '28'
            ],
            [
                'id' => '27',
                
                'descricao' => 'Tocantins',
                'sigla' => 'TO',
                'codigo_ibge' => '17'
            ]
        ]);
    }
}