<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImovelTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'imovel';

    /**
     * Run the migrations.
     * @table imovel
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('titulo', 45)->nullable()->default(null);
            $table->integer('imovel_tipo_id')->unsigned();
            $table->integer('cidade_id')->unsigned();
            $table->string('cep', 10)->nullable()->default(null);
            $table->decimal('preco', 11, 2)->nullable()->default(null);
            $table->integer('area')->nullable()->default(null);
            $table->integer('dormitorios')->nullable()->default(null);
            $table->integer('suites')->nullable()->default(null);
            $table->integer('banheiros')->nullable()->default(null);
            $table->integer('salas')->nullable()->default(null);
            $table->integer('garagem')->nullable()->default(null);
            $table->text('descricao')->nullable()->default(null);
            $table->string('bairro', 150)->nullable()->default(null);
            $table->integer('numero')->nullable()->default(null);
            $table->string('complemento')->nullable()->default(null);
            $table->string('logradouro')->nullable()->default(null);
            $table->integer('imovel_status_id')->unsigned();

            $table->index(["imovel_tipo_id"], 'fk_imovel_imovel_tipo_idx');

            $table->index(["imovel_status_id"], 'fk_imovel_imovel_status1_idx');

            $table->index(["cidade_id"], 'fk_imovel_cidade1_idx');
            $table->timestamps();


            $table->foreign('cidade_id', 'fk_imovel_cidade1_idx')
                ->references('id')->on('cidade')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('imovel_status_id', 'fk_imovel_imovel_status1_idx')
                ->references('id')->on('imovel_status')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('imovel_tipo_id', 'fk_imovel_imovel_tipo_idx')
                ->references('id')->on('imovel_tipo')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
