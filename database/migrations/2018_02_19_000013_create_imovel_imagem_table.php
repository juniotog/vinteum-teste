<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImovelImagemTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'imovel_imagem';

    /**
     * Run the migrations.
     * @table imovel_imagem
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->binary('descricao')->nullable()->default(null);
            $table->integer('imovel_id')->unsigned();

            $table->index(["imovel_id"], 'fk_imovel_imagem_imovel1_idx');


            $table->foreign('imovel_id', 'fk_imovel_imagem_imovel1_idx')
                ->references('id')->on('imovel')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
        DB::statement("ALTER TABLE `imovel_imagem` CHANGE COLUMN `descricao` `descricao` MEDIUMBLOB NULL AFTER `id`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
