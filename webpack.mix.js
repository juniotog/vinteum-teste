let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css')
.combine([
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/angular/angular.min.js',
    'bower_components/angular-i18n/angular-locale_pt-br.js',
    'bower_components/bootstrap/dist/js/bootstrap.min.js',
    'bower_components/admin-lte-angular/admin-lte-angular.min.js',
    'bower_components/angular-route/angular-route.min.js',
    'bower_components/angular-loading-bar/build/loading-bar.min.js',
    'node_modules/angular-input-masks/releases/angular-input-masks-standalone.min.js',
    'bower_components/angular-upload/angular-upload.min.js',
    'node_modules/pnotify/dist/iife/PNotify.js'
], 'public/js/vendor.js')
.combine([
    'resources/assets/angularjs/app.js',
    'resources/assets/angularjs/configs.js',
    'resources/assets/angularjs/directives.js',
    'resources/assets/angularjs/controllers/login.controller.js',
    'resources/assets/angularjs/controllers/main.controller.js',
    'resources/assets/angularjs/controllers/imovel.controller.js',
    'resources/assets/angularjs/services/imovel.service.js',
    'resources/assets/angularjs/services/autenticar.service.js',
    'resources/assets/angularjs/services/usuario.service.js',
    'resources/assets/angularjs/services/imagem.service.js',
], 'public/js/app.js')
.copy('node_modules/admin-lte/dist/img/avatar.png', 'public/images/vendor/admin-lte/dist/img/avatar.png')
.copy('resources/assets/images/ajax-loading-mini.gif', 'public/images/ajax-loading-mini.gif')
.copyDirectory('resources/assets/angularjs/views', 'public/views')
.options({ processCssUrls: false });
