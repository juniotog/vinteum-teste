# Sobre o projeto #

Teste feito para o processo seletivo da empresa vinteum

## Instala��o ##
Seguir um passo de cada vez, em ordem.
~~~~
git clone git@bitbucket.org:juniotog/vinteum-teste.git
~~~~
~~~~
composer install
~~~~
~~~~
npm install
~~~~
~~~~
bower install
~~~~
~~~~
renomeiar o .env.example para .env e configurar os dados do banco de dados (dever� criar um banco)
~~~~
~~~~
php artisan migrate
~~~~
~~~~
php artisan db:seed
~~~~
~~~~
php artisan key:generate
~~~~
~~~~
php artisan passport:install
~~~~
~~~~
Copiar o Client Secret do Client ID 2, gerado pelo comando anterior e colocar no arquivo: \vinteum-teste\resources\assets\angularjs\services\autenticar.service.js
~~~~
Exemplo:
![Alt text](http://i67.tinypic.com/ir2jvm.png)
~~~~
npm run production
~~~~
~~~~
e por fim o comando: php artisan serve
~~~~
